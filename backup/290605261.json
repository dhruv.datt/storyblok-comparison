{
  "name": "What Is a Rainy Day Fund?",
  "created_at": "2023-04-12T09:56:56.604Z",
  "published_at": "2023-02-28T19:52:06.091Z",
  "id": 290605261,
  "uuid": "184f519b-c9de-46b2-816c-dffbefe61b67",
  "content": {
    "_uid": "b541b0b8-a176-42c9-80c4-f11c0fa00058",
    "body": [
      {
        "_uid": "31942b10-4aef-42da-ad7c-4316dae6a223",
        "title": "Related Content",
        "component": "contentInfoCards",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "contentInfoCard": [
          {
            "_uid": "3f426252-3202-42f0-abd3-6b85797e5da3",
            "link": {
              "id": "21c28523-2a18-4cc7-a066-6dc10a817c82",
              "url": "",
              "linktype": "story",
              "fieldtype": "multilink",
              "cached_url": "savings/how-to-save-your-money"
            },
            "image": [
              {
                "alt": "stack of dollars with a padlock",
                "img": "//a.storyblok.com/f/180759/1920x1279/73b2f45b2a/places-to-save.jpg",
                "_uid": "12163f41-07bb-480b-a9cb-0f9fd19f36ea",
                "mobile": "",
                "tablet": "",
                "desktop": "",
                "component": "media"
              }
            ],
            "title": "Places to Save Your Extra Money",
            "target": "_self",
            "component": "contentInfoCard",
            "buttonText": "Learn More",
            "description": ""
          },
          {
            "_uid": "5cf5131f-02c8-4a1f-8d2c-d09ea2b3de1d",
            "link": {
              "id": "ddef43b7-9f75-4931-b791-53f5ec284224",
              "url": "",
              "linktype": "story",
              "fieldtype": "multilink",
              "cached_url": "savings/starting-an-emergency-fund"
            },
            "image": [
              {
                "alt": "",
                "img": "//a.storyblok.com/f/70134/800x533/fd6e85b522/emergency-fund_tile.png",
                "_uid": "d86cce77-4497-4be9-802b-900097e8d114",
                "mobile": "",
                "tablet": "",
                "desktop": "",
                "component": "media"
              }
            ],
            "title": "Starting an Emergency Fund",
            "target": "_self",
            "component": "contentInfoCard",
            "buttonText": "Learn More",
            "description": ""
          },
          {
            "_uid": "fc9a9e1a-3544-4b04-93b6-d76adbd09720",
            "link": {
              "id": "3a0182b3-7531-4f12-af8e-e29c454f9ca7",
              "url": "",
              "linktype": "story",
              "fieldtype": "multilink",
              "cached_url": "savings/how-to-save-money-fast"
            },
            "image": [
              {
                "alt": "Dog in moving car with nose out window",
                "img": "//a.storyblok.com/f/70134/800x537/bf1e429eb1/how-to-save-money-fast_tile.jpg",
                "_uid": "f7fec5a0-0125-43d4-99b8-58901bcab6cd",
                "mobile": "",
                "tablet": "",
                "desktop": "",
                "component": "media"
              }
            ],
            "title": "How to Save Money Fast",
            "target": "_self",
            "component": "contentInfoCard",
            "buttonText": "Learn More",
            "description": ""
          }
        ],
        "hasPaddingBottom": false
      }
    ],
    "logo": [],
    "title": "What Is a Rainy Day Fund?",
    "layout": {
      "id": "d64b2f44-1440-4125-882c-1ac2928c5067",
      "url": "",
      "linktype": "story",
      "fieldtype": "multilink",
      "cached_url": "layouts/explore-product-layout"
    },
    "metadata": {
      "_uid": "4f1bed23-d524-424e-8c41-86d2a506fb97",
      "title": "What Is a Rainy Day Fund? | SaveBetter",
      "plugin": "meta-fields",
      "description": "A rainy day fund is crucial if you don’t want sudden expenses to throw your budget into disarray. Learn how to start saving and grow one to meet unforeseen expenses."
    },
    "component": "page",
    "isTextWhite": true,
    "headerOutline": [
      {
        "_uid": "b09fb69a-9ae3-4ca3-bb9d-9fc32c9ff8be",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you’ve ever faced an unexpected car repair bill or had to make a sudden, costly visit to the dentist, you may have realized the importance of a rainy day fund. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "As the name suggests, a rainy day fund is a sum of money set aside to cover one-off expenses like a broken windshield or a sensitive tooth. The costs involved may not be exorbitant, but they still might be significant enough to disrupt your monthly budget. Enter the rainy day fund: an accessible source of money that will cover these types of unforeseen expenses so they don’t throw off your financial balance.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "But how does a rainy day fund compare with an emergency fund? An emergency fund is for major expenses brought on by serious events like illnesses that require hospitalization or cause a sudden loss of income. A rainy day fund, on the other hand, is for less serious events that involve significantly lower costs. Ideally, a rainy day fund should be able to cover sudden expenses that fall outside your monthly budget.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Having a rainy day fund is the financial equivalent of keeping an umbrella handy at all times. With adequate cover, you’ll never be stuck struggling to pay bills. So, let’s wade into the details of a rainy day fund.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "609bec0d-a32c-40f2-8b20-f4d6ec2b6e20",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "bullet_list",
              "content": [
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Benefits of a rainy day fund",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "What type of expenses should a rainy day fund cover? ",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "How much money should my rainy day fund have?",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Rainy day saving strategies",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        "title": "What's on this page?",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "b853e917-1385-4b2f-bee2-9cc872cbe32d",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Small financial shocks can add up and make a big dent in your long-term financial goals. A rainy day fund is an additional safety net that can help you cover these sudden expenses easily.  ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Having a rainy day fund gives you a certain degree of flexibility in case you have to outlay money for something not in your typical budget. It helps you avoid resorting to costlier alternatives like borrowing money, such as through a personal loan; withdrawing early from a CD account or stock portfolio holding long-term savings; or using a credit card. With a separate rainy day fund, you can avoid dipping into investments or other savings like an emergency fund. ",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Benefits of a rainy day fund",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "43a9b4bd-cb83-4d8c-99ab-8a86b517de12",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Trouble comes calling when you least expect it. All you can do is be prepared. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Sudden expenses are also similarly unexpected. A rainy day fund should be able to cover a variety of unforeseen expenses like the following:",
                  "type": "text"
                }
              ]
            },
            {
              "type": "bullet_list",
              "content": [
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Replacing a broken kitchen appliance",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        { "text": "New tires for your car", "type": "text" }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "A visit to the doctor, dentist, or veterinarian",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Replacing your mobile phone if it gets lost or stolen",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        { "text": "Minor home repairs ", "type": "text" }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [{ "text": "A sudden trip ", "type": "text" }]
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "These are just examples; many other kinds of situations can crop up. A rainy day fund feels most useful when events occur that you couldn’t have fathomed.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "What type of expenses should a rainy day fund cover? ",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "e89ca203-f5e1-4cae-ab7d-2ea294a12a17",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "There are no strict guidelines about the amount of money you should put aside in a rainy day fund. While $500 to $2,500 is a ballpark figure, the actual figure should be determined by your lifestyle, financial circumstances, and your number of dependents.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You can come up with an optimal figure through a quick assessment of things that could potentially go wrong in your daily life and a rough calculation of how much it would cost to fix or address those issues. Consider, for instance, what it would cost to get a new refrigerator or water heater—household items that are hard to live without—if yours were to suddenly break down. This will ensure that your rainy day fund has enough to meet surprise expenses without throwing your budget out of gear.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "How much money should my rainy day fund have?",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "5bab2266-428c-4af3-8c1e-c7cbd1bd7ba8",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Since the expenditures we are discussing will be unexpected, it’s smart to keep your rainy day funds easily accessible.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "The safest bet is a ", "type": "text" },
                {
                  "text": "high-yield savings account",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/savings-accounts",
                        "uuid": null,
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": ", which will ensure you maintain liquidity and easy access to your funds while getting a higher interest rate than you otherwise would with a checking account, for example. You can also look at ",
                  "type": "text"
                },
                {
                  "text": "money market deposit accounts",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/money-market-accounts",
                        "uuid": null,
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " (MMDAs) for your rainy-day funds, as these types of accounts also offer strong earnings potential and liquidity. Plus, by keeping your rainy day fund in a separate account from that of your daily spending money, you’ll be less likely to dip into it for discretionary purchases.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Look for an account that is insured by either the Federal Deposit Insurance Corporation (",
                  "type": "text"
                },
                {
                  "text": "FDIC",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.fdic.gov/",
                        "uuid": null,
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": ") or the National Credit Union Administration (",
                  "type": "text"
                },
                {
                  "text": "NCUA",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.ncua.gov/",
                        "uuid": null,
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": "). For example, all financial institutions on the online savings platform SaveBetter are insured by the FDIC or NCUA.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Ideally, keep your rainy day fund in an account that allows quick withdrawals without additional charges. Keeping your money in separate accounts can be helpful in certain circumstances.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Why might interest rates on savings accounts change?",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "b8965b66-7e8c-4dc7-b245-38ff87f84f86",
        "cards": [
          {
            "_uid": "51741bfd-2844-4053-8fab-bfbcbdfebf53",
            "title": "Determine the size of your umbrella",
            "component": "howItWorksCard",
            "description": "Figure out how large your rainy day fund needs to be by listing all the little things that could go wrong at a moment's notice and then estimating your cash outflow if two such expenses came up in the same month. This exercise will give you a rough idea of the amount you’ll need to stash away for your rainy day fund. You’ll need to budget even higher if you have children."
          },
          {
            "_uid": "d41e7925-251c-4c52-abd9-361044b8084a",
            "title": "Start small",
            "component": "howItWorksCard",
            "description": "If your cash situation is tight, start by putting away small amounts. Even saving just $5 per day adds up to $150 at the end of the month. Having even a little something to fall back on for a sudden expense can help you avoid financial stress. Any extra income, windfall, or unexpected savings should go straight to your rainy day fund."
          },
          {
            "_uid": "03ae5e9f-2bcc-4d08-90c1-99795b72504d",
            "title": "Include rainy day savings in your budget",
            "component": "howItWorksCard",
            "description": "Expenses vary every month, but saving for your rainy day fund can’t be left to chance. Factoring your rainy day fund contribution into your monthly budget will help you reach your savings target. Try breaking down your monthly savings goal by the week, making it easier to achieve."
          },
          {
            "_uid": "60bc51c1-e3f3-4e23-ae8d-f612f5662317",
            "title": "Develop discipline",
            "component": "howItWorksCard",
            "description": "There are many ways you can cut down on expenses to contribute more towards savings, including your rainy day fund. Having home-cooked meals over eating out can save you several hundred dollars a month. Letting go of unnecessary magazine or streaming subscriptions is another great way to save a few dollars every month. Once you get into the habit, you can easily find multiple ways of saving small amounts that can add up in a big way."
          },
          {
            "_uid": "e741f2e3-1340-4711-83a6-1afa7c29bc8d",
            "title": "Get the right account",
            "component": "howItWorksCard",
            "description": "Find a savings account with no monthly fees or restrictions on withdrawals — such as those found on SaveBetter. Having multiple savings buckets within the same account can also be helpful. Opt for a high-yield savings account so that your money accumulates interest while being readily accessible at all times. "
          },
          {
            "_uid": "855ee847-a090-4679-be06-32fd902fddb1",
            "title": "Systematize your savings",
            "component": "howItWorksCard",
            "description": "Figure out how much you can easily save from your monthly paycheck. You can start with as little as 2% or 5% of your income and create a monthly automatic transfer for that amount to a savings account. Short of that, you can set a recurring reminder in your digital calendar to alert you each month to sock away those savings. This will alleviate the hassle of remembering to transfer and also make it easier to avoid unnecessary expenses that could eat into your monthly budget."
          },
          {
            "_uid": "56619ea9-e73b-42d6-8dcd-f94fb5d9f731",
            "title": "Increase your savings rate over time",
            "component": "howItWorksCard",
            "description": "Dial up the percentage of your monthly savings that you invest in your rainy day fund if you get a raise or move to a higher-paying job. Putting aside more money towards a rainy day fund will greatly increase your short-term financial stability and shield you from piling on debt to meet expenses."
          },
          {
            "_uid": "e1990efd-8a76-46a6-92ca-c5b75e1810b9",
            "title": "Create multiple rainy day funds",
            "component": "howItWorksCard",
            "description": "Don’t stop once your initial savings goal has been met. Instead of having one rainy day fund to cover all eventualities, you can now create separate funds: for your children, pets, home, car, medical, and miscellaneous expenses. That way, you’ll always have something saved to meet a specific type of expense."
          }
        ],
        "media": [],
        "title": "Rainy day saving strategies",
        "component": "howItWorks",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "4836d1da-49fb-4069-8144-03239dd68796",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Start your rainy day fund with a secure account that gives you the convenience and flexibility you need. ",
                  "type": "text"
                },
                {
                  "text": "SaveBetter",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/savings/rainy-day-fund/",
                        "uuid": null,
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " offers unbeatable options to start your rainy day fund. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "All our savings products are federally insured, and our high-yield savings accounts offer some of the most competitive interest rates available nationally.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Get started with your rainy day fund",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "30f5fde1-f094-4585-b13e-7fb1126c3961",
        "href": "https://www.savebetter.com/explore-products",
        "kind": "primary",
        "size": "medium",
        "text": "Register today",
        "target": "_self",
        "justify": "justify-center",
        "rounded": "rounded",
        "disabled": false,
        "component": "button",
        "hasBackground": false,
        "hasPaddingTop": false,
        "backgroundColor": "",
        "hasPaddingBottom": false
      }
    ],
    "disableIndexing": false,
    "showBreadcrumbs": true,
    "headerBackground": [
      {
        "alt": "",
        "img": "//a.storyblok.com/f/180759/2560x550/8b581d1686/normal_u13.png",
        "_uid": "34cdd729-891d-4d98-a585-311e8cd07a22",
        "mobile": "",
        "tablet": "",
        "desktop": "",
        "component": "media"
      }
    ],
    "introductionLink": [],
    "introductionList": [],
    "isLinkSecondaryColor": false,
    "introductionDescription": "All you need to know about a rainy day fund, from how to start it to getting the most out of it. "
  },
  "slug": "rainy-day-fund",
  "full_slug": "savings/rainy-day-fund",
  "sort_by_date": null,
  "position": -10,
  "tag_list": [],
  "is_startpage": false,
  "parent_id": 290604910,
  "meta_data": null,
  "group_id": "e3679e18-44c2-45e6-bc82-0c7b020fd651",
  "first_published_at": "2022-09-23T19:14:01.752Z",
  "release_id": null,
  "lang": "default",
  "path": null,
  "alternates": [],
  "default_full_slug": null,
  "translated_slugs": null
}

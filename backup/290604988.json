{
  "name": "How much do you need to save for retirement? Here are some rules of thumb",
  "created_at": "2023-04-12T09:56:26.377Z",
  "published_at": "2023-02-14T16:01:02.141Z",
  "id": 290604988,
  "uuid": "a8de7ab4-7bbd-4b3c-bee5-548b49edf8d5",
  "content": {
    "_uid": "8a7065fb-85d1-4776-a960-b899c3661076",
    "body": [
      {
        "_uid": "5dcc7352-3473-4599-ae01-0097fc790e52",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Everyone dreams about living comfortably in their later years of life. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "But whether you have big plans to travel the country in an RV, or put down new roots by the ocean, getting there will take some planning. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The Covid-19 pandemic, and the economic uncertainty it caused, made this ever more clear for many Americans. In fact, more than two-thirds of participants (68%) in a recent ",
                  "type": "text"
                },
                {
                  "text": "SaveBetter survey",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/consumer-survey-results-03-2021",
                        "uuid": "daa265ba-7bc5-4ab8-9806-af8660d14d31",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": ", agreed that the Covid-19 pandemic made them realize the importance of an ",
                  "type": "text"
                },
                {
                  "text": "emergency fund and nest egg",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/build-nest-egg-in-nutshell",
                        "uuid": "cd754b71-b3af-4a08-b2c6-11188d6b0105",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": ".", "type": "text" }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "That same percentage of respondents also admitted recent events raised their concerns about saving for retirement. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Unfortunately, awareness about the need to save more for your golden years is only half the battle; the other half is actually setting a plan in motion. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "But where do you begin? And how much will you need? It turns out, ",
                  "type": "text"
                },
                {
                  "text": "more than half",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.cnbc.com/2019/07/12/americans-dont-know-how-much-they-need-to-retire.html",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " of non-retirees aren’t sure. So, it’s time to clarify just how much you should plan to save, so you can eventually sit back and enjoy your retirement just the way you want.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "b71e6457-2953-490c-ae32-9ef9c7f79e77",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Figuring out how much money you need to retire can feel daunting. Luckily, experts have developed guidelines to help. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Personal finance expert and Certified Financial Planner Liz Weston ",
                  "type": "text"
                },
                {
                  "text": "suggests",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://business.time.com/2012/12/28/9-handy-financial-rules-of-thumb/",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " setting aside 10% of your pay for retirement to cover the basic needs, 15% if you want to live comfortably, and 20% if you really want to escape.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "To help you visualize what that might equate to over time, here’s an example using this advice:",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Consider a couple who earns a collective $75,000 per year in salaries. They decide to save 15% of their wages for retirement. Both start at age 25 and continue to save 15% of their salaries annually for 40 years. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "At age 65, the couple will have built a retirement nest egg worth $1,430,643, assuming a 5% annual return. And this doesn’t include any Social Security benefits for which they might qualify. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Others may prefer to look at saving for retirement from a benchmark standpoint. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "Some ", "type": "text" },
                {
                  "text": "investment management companies",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.fidelity.com/viewpoints/retirement/how-much-do-i-need-to-retire",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " suggest using your age and salary to figure out how much you should already have saved for retirement. Here’s what they recommend:",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "How much do you need to retire? These rules of thumb may help provide answers",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "93adb9ff-d35b-4cc4-ba8d-5ecee63f6989",
        "media": [
          {
            "alt": "Age 30 - 1x your salary; Age 35 - 2x your salary; Age 40 - 3x your salary; Age 45 - 4x your salary; Age 50 - 6x your salary; Age 55 - 7x your salary; Age 60 - 8x your salary; Age 67 - 10x your salary",
            "img": "//a.storyblok.com/f/70134/1500x800/e26c3827b4/how-much-to-save-for-retirement.svg",
            "_uid": "6c2d9c16-0ede-4638-af49-582acde1d5c2",
            "component": "media"
          }
        ],
        "component": "blogPostImage",
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "dd5c9751-eb57-403d-a420-4eac16cc4f6b",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Aside from these rules of thumb, there is plenty more advice out there on just how much you’ll need for retirement. But remember, these are only recommendations and may not be the perfect fit, as your personal financial circumstances are as unique as you are. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You may wish to consider consulting with a financial adviser or professional to guide you, or commit you to a savings plan that’s tailor-made for you.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "ca5393aa-e133-491a-8bbf-19fa06bed136",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "While rules of thumb are helpful, they don’t replace running the numbers for yourself. You can use a retirement calculator to see how much money you could save if you start investing today.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "Some ", "type": "text" },
                {
                  "text": "retirement calculators",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.nerdwallet.com/investing/retirement-calculator",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " allow you to plug in several basic variables to see if you’re on track for retirement. If you want to dive into more specifics, this ",
                  "type": "text"
                },
                {
                  "text": "free retirement calculator",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.merrilledge.com/retirement/personal-retirement-calculator",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " can help you dial into the details.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Don’t forget to consider any Social Security benefits you might receive in the future. You can use the ",
                  "type": "text"
                },
                {
                  "text": "Social Security Administration’s calculator",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.ssa.gov/oact/quickcalc/",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " to estimate how much you may be eligible to earn.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Make sure you’re factoring in all aspects of your retirement plan before settling on how much money you really need to retire the way you want. There’s no right or wrong way to come up with this number, so take your time to figure out the best plan for you.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Use calculators to help",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "25e95cfb-db79-44f5-b056-90ec9fb827ce",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You’ve likely heard the saying, “Don’t put all your eggs in one basket.” And the same is true for your investments. This strategy, known as diversification, is when you spread your investments and savings out among multiple assets and investment vehicles to manage risk more effectively. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you’ve already got a 401(K), IRA, or other types of brokerage and savings accounts — or you plan to open them soon — you’ll want to ensure your savings are diversified.  ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Diversification may help your portfolio weather financial storms because if one asset is doing poorly, another might still be doing well. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "For example, if you’re 100% invested in the S&P 500 index, but it drops 40% the year before you retire, you may have to delay retirement. On the other hand, if you spread out your investments into various vehicles, your portfolio may dip slightly if a portion was within the S&P 500, however, your other well-performing assets could help balance the scales.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "After you’ve determined your “number,” focus on savings and diversification",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "b9889dd3-dab9-4e87-bf47-8b97b3276241",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "In addition to investment accounts, having cold-hard cash within your portfolio can provide some ",
                  "type": "text"
                },
                {
                  "text": "much-needed stability",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/savings-accounts-for-retirement-strategy",
                        "uuid": "aeba5b8e-b546-4213-b542-8f0da7b946bf",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " as you invest for, get closer to, and enter retirement. Cash and cash equivalents usually refer to money in vehicles such as ",
                  "type": "text"
                },
                {
                  "text": "savings accounts",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/using-high-yield-savings-account-to-grow-your-money-faster",
                        "uuid": "ecd128cb-5f36-490c-ada5-630dac0a4d9c",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": ", money market accounts, and ", "type": "text" },
                {
                  "text": "certificates of deposit",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/certificates-of-deposit-guide",
                        "uuid": "ba5ef813-5172-422d-8b51-a1d789aff333",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": " (CDs). ", "type": "text" }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Cash savings are important because, unlike investments, they do not come with risks. Instead, the money you deposit into your account is guaranteed to earn you interest over time and is even FDIC-insured. This means you can rest easy knowing your money is secure from everything from bank failure to economic uncertainty. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "When it comes to keeping cash in your portfolio, the key is finding the best possible return on your money. You may do so by ",
                  "type": "text"
                },
                {
                  "text": "diversifying your savings ",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/diversify-your-cash-savings",
                        "uuid": "71cdfed8-9d55-4294-b8f5-9088823678e8",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": "throughout multiple high-yield savings products. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Here are some savings products to consider and the benefits and features of each:",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Why you should consider cash as part of your retirement plan",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "e6c0e779-e0b1-422a-961d-6bee9031fbb7",
        "media": [
          {
            "alt": "",
            "img": "//a.storyblok.com/f/180759/1027x883/1be98036a8/different-product-types.png",
            "_uid": "6d40e4c2-5b31-4b9b-bc73-2b1502e33915",
            "component": "media"
          }
        ],
        "component": "blogPostImage",
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "dbd1046b-8940-4768-a3e7-dbc12a9d1159",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "SaveBetter provides an easy solution to help ensure your cash savings are earning as much as possible without the headaches of opening and managing multiple bank accounts separately. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "You can sign up for ", "type": "text" },
                {
                  "text": "SaveBetter",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/savebetter-com-a-better-way-to-reach-your-savings-goals",
                        "uuid": "2015bc7d-24bc-42b0-8cec-f6c94b26874f",
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " in just a few minutes and get access to high-yield savings accounts and CDs from SaveBetter’s partner banks all in one place. Your money is covered by FDIC insurance at each institution, but you only have to visit SaveBetter to view and manage all of your open banking accounts. SaveBetter even makes things easier at tax time by compiling your ",
                  "type": "text"
                },
                {
                  "text": "1099-INTs in one place",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/4-ways-savebetter-will-help-you-smash-your-savings-goals",
                        "uuid": "73eb2f1c-30b6-4735-bb7a-668a1505793e",
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": ". ", "type": "text" }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Sign up for a SaveBetter account",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/how-to-get-started-with-savebetter",
                        "uuid": "a9752f1e-b83c-410d-8b0f-befd5b19ca7c",
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " today to see how easy it is to maximize your returns and manage your money all in one place.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Diversifying your money is easy with SaveBetter",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "f122c219-1b40-4518-ba0a-cdccecfccae3",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The Top 5 Factors People Care Most About When Opening a Savings Account",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/top-5-factors-savings-account",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Open Joint Accounts with SaveBetter",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/how-to-open-joint-accounts",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Why a money market account makes sense for your savings",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/what-is-a-money-market-account",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Make a Money-Saving Resolution That Will Actually Stick",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/stick-to-your-money-saving-resolution",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Why Savings Accounts Are Important, Even When Rate Are Low",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/why-people-save-even-when-rates-are-low",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        "title": "Related Articles",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": true,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      }
    ],
    "image": [
      {
        "alt": "A caculator and some documents",
        "img": "//a.storyblok.com/f/70134/1584x800/3cc110e59d/how-much.jpg",
        "_uid": "d1d96d3a-4866-4885-ba0c-551c6b90539a",
        "component": "media"
      }
    ],
    "intro": "",
    "title": "How much do you need to save for retirement? Here are some rules of thumb",
    "metadata": {
      "_uid": "ec810ca0-4250-4441-a610-1eb86a140eea",
      "title": "How much do you need to save for retirement? Here are some rules of thumb | SaveBetter",
      "plugin": "meta-fields",
      "description": "Figuring out how much money you need to retire can feel daunting. Luckily, experts have developed guidelines to help. "
    },
    "component": "blogPostPage",
    "categories": [
      "12716529-9eb5-408c-b8c8-8f5355832667",
      "6305ef4d-a589-447a-a4ca-5293dc446796"
    ],
    "disableIndexing": false,
    "showBreadcrumbs": false,
    "blogPostEndingSection": [
      {
        "_uid": "c93e16f1-1e6f-49f8-b8f3-96a1a6a6d6a0",
        "links": ["Facebook", "LinkedIn", "Twitter2", "Email"],
        "footNote": "",
        "component": "blogPostEndingSection",
        "hasBorder": false,
        "sectionTitle": "Share this article",
        "hasBackground": false,
        "hasPaddingTop": true,
        "hasPaddingBottom": true
      }
    ]
  },
  "slug": "how-much-ro-save-for-retirement",
  "full_slug": "blogs/how-much-ro-save-for-retirement",
  "sort_by_date": null,
  "position": 20,
  "tag_list": [],
  "is_startpage": false,
  "parent_id": 290604867,
  "meta_data": null,
  "group_id": "925ffaaa-967e-4dd5-8927-12487a2fe6e8",
  "first_published_at": "2020-12-23T22:18:03.237Z",
  "release_id": null,
  "lang": "default",
  "path": null,
  "alternates": [],
  "default_full_slug": null,
  "translated_slugs": null
}

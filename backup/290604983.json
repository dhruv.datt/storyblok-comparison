{
  "name": "Why Savings Accounts Should Be Part of Your Retirement Strategy ",
  "created_at": "2023-04-12T09:56:26.377Z",
  "published_at": "2022-02-18T10:53:54.638Z",
  "id": 290604983,
  "uuid": "aeba5b8e-b546-4213-b542-8f0da7b946bf",
  "content": {
    "_uid": "8a7065fb-85d1-4776-a960-b899c3661076",
    "body": [
      {
        "_uid": "5dcc7352-3473-4599-ae01-0097fc790e52",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "When you go on vacation, you wouldn’t pack just one outfit. Instead, you plan for a variety of circumstances — a swimsuit for the beach, shorts for warm days, and even a jacket in case of a surprise storm. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The same rings true when it comes to your retirement planning. You want to ensure your savings cover all of your needs, and that you plan for any surprises along the way.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "For example, your current retirement portfolio may include a combination of tax-advantaged accounts, such as a 401(k) or IRA, and other investments, like a brokerage account that includes stocks and bonds. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "While these are all great tools for building wealth over the long term, you may also want to consider cash as an integral part of your diversified retirement strategy, and here’s why.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "b71e6457-2953-490c-ae32-9ef9c7f79e77",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Of course, there are many types of investments you can include in your retirement portfolio if you want to grow your money. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "For example, you could opt for real estate (think investment properties or real estate investment trusts, aka REITs), individual stocks, or even bond investments, each providing an opportunity to exponentially grow your contributions over time. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "However, each of these investments also carries risk, and returns are not guaranteed. In fact, you could even lose money. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "In contrast, a savings account offers stability without the risk. Cash that’s readily accessible in savings accounts can be a great place to put your short-term spending money as well as an emergency fund. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "By including cash in your portfolio, you can better balance your overall level of risk. This helps you adjust to where you are in your retirement journey while still working toward ",
                  "type": "text"
                },
                {
                  "text": "your financial goals",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/savebetter-com-a-better-way-to-reach-your-savings-goals",
                        "uuid": "2015bc7d-24bc-42b0-8cec-f6c94b26874f",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": ".", "type": "text" }
              ]
            }
          ]
        },
        "title": "How cash fits into your portfolio",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "dce60616-0e3c-4100-9255-04297a3f5c92",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "According to a recent SaveBetter survey, “The Impact of the Covid-19 Pandemic on American Consumers’ Savings and Banking Behaviors,” ",
                  "type": "text"
                },
                {
                  "text": "more than two-thirds",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/consumer-survey-results-03-2021",
                        "uuid": "daa265ba-7bc5-4ab8-9806-af8660d14d31",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " (68%) of participating adults said that the pandemic had raised their awareness surrounding the need for a healthy ",
                  "type": "text"
                },
                {
                  "text": "nest egg",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/build-nest-egg-in-nutshell",
                        "uuid": "cd754b71-b3af-4a08-b2c6-11188d6b0105",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": ". ", "type": "text" }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The same percentage of employed adults also agreed that recent events had boosted their concerns about saving for retirement. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Within your portfolio, you’ll want access, safety, and flexibility, and no other asset offers the best of these benefits as well as cash. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "But cash saved in a low or non-interest bearing account may actually lose value over time with the effects of inflation. So, you may want to seek out interest-bearing savings products to ",
                  "type": "text"
                },
                {
                  "text": "help you meet short-term goals",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/4-ways-savebetter-will-help-you-smash-your-savings-goals",
                        "uuid": "73eb2f1c-30b6-4735-bb7a-668a1505793e",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " like high-yield savings accounts and certificates of deposit (CDs). ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "With these types of savings products, you’ll benefit from interest rates that are generally higher than traditional checking and savings accounts.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Cash and your retirement",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "80576650-72f3-4058-909f-8ce854f96d17",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Let’s dive a bit deeper into the key benefits that savings accounts provide, which you won’t find with investment vehicles.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Access: Get your money fast",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you need to get to your money quickly, the funds in most savings accounts are readily available. You won’t incur penalties for withdrawing this cash, and you won’t have to jump through hoops to get it.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "This is quite different from trying to access money held in a brokerage account, where it can take days to get funds once you submit a withdrawal request. Depending on the type of account, you may also be subject to fees, like early withdrawal penalties.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Real estate investments can be even trickier. These funds are often held up for years, with most of your value tied to equity. To access this money, you’d need to take out an equity loan or even sell the property.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Security: Your balance is safe, secure, and insured",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Savings accounts are generally a safe place to put your money without fear of loss. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Money held in a savings account has nearly zero inherent risks. You earn interest on the balance, and that rate can fluctuate with the market — but your savings account deposits (plus accrued interest) can never lose value, no matter what happens to the economy.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Savings accounts are also typically covered by ",
                  "type": "text"
                },
                {
                  "text": "FDIC insurance",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.fdic.gov/deposit/deposits/",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": ". Unlike invested funds, the money held in an FDIC-protected deposit account is insured up to $250,000 per depositor, per banking institution, per deposit category. So, if your bank goes under, for instance, your savings would still be protected.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "And with many banks and financial institutions now implementing SOC 2 security controls, you also have peace of mind that your personal data is protected, too.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Flexibility: Savings options to meet your needs",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Many online banks and platforms offer both ",
                  "type": "text"
                },
                {
                  "text": "high-yield savings accounts",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/using-high-yield-savings-account-to-grow-your-money-faster",
                        "uuid": "ecd128cb-5f36-490c-ada5-630dac0a4d9c",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": " (HYSA) and ", "type": "text" },
                {
                  "text": "certificates of deposit",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/certificates-of-deposit-guide",
                        "uuid": "ba5ef813-5172-422d-8b51-a1d789aff333",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " (CDs), so you can select an account, or accounts, that meet your specific needs. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "High-yield savings accounts allow you to regularly contribute and withdraw funds as needed, making them perfect for short-term or emergency savings.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "CDs usually offer ", "type": "text" },
                {
                  "text": "higher returns",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/how-apy-helps-your-savings-grow",
                        "uuid": "ca6f656e-b260-43de-a92c-bfd3d4e193b9",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " in exchange for leaving that money untouched for an agreed-upon period of time. If you don’t plan to use that cash for a while — but still want the ability to withdraw early if absolutely necessary — a CD (or several) can be a great choice. Note that traditional CDs may implement an early withdrawal fee if you remove your funds before the maturity date, so consider if and when you may need that money before selecting a CD time frame. That said, no-penalty CDs also exist, though they typically offer lower returns.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Why savings accounts are important",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "485edd23-c381-4522-8f59-8264a2669e0d",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The amount of money you may want to keep in savings versus what you allocate for investments will depend on your unique situation and will likely even change as time goes on.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "One significant determining factor is how close you are to retirement. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you’re already retired, consider a notable cash reserve of around 24 to 36 months’ worth of expenses. This amount can help support your short-term spending needs as well as provide you with a safety net in case of emergencies or increased medical costs.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you’re nearing retirement, it’s wise to keep about 12 months’ worth of expenses on hand. In the event the market dips, or you encounter a sudden expense, this cash may save you from making a costly withdrawal from an investment account.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "And, of course, cash is also important earlier in life and throughout your retirement-saving journey. Many financial experts recommend somewhere between three and six months’ worth of expenses in an accessible savings account, which can keep you afloat if you become ill, get injured, or lose your job. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you’re unsure of the right cash reserves or asset mix for your needs, consider talking to a financial adviser who can review your specific personal and financial circumstances.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "How much cash do you need in savings?",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "9d192fdd-44fe-4bc6-aac2-21e6e6770e67",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "In addition to involving cash in your retirement strategy, you’ll want to consider ",
                  "type": "text"
                },
                {
                  "text": "diversifying those savings",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/diversify-your-cash-savings",
                        "uuid": "71cdfed8-9d55-4294-b8f5-9088823678e8",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": ". You can tackle this in a few different ways.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "You can use a combination of ", "type": "text" },
                {
                  "text": "HYSAs and CDs",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/difference-between-high-yield-savings-accounts-and-cds",
                        "uuid": "aceca058-d3b3-4061-9138-b9201199f921",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " to maximize earning potential while still accounting for both short- and long-term goals. For example, cash that you can afford to lock away for a while might be well placed in a CD, while the money you could need at any time may go into an HYSA. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "This is one of the benefits of ", "type": "text" },
                {
                  "text": "using the SaveBetter platform",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/how-to-get-started-with-savebetter",
                        "uuid": "a9752f1e-b83c-410d-8b0f-befd5b19ca7c",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": ", by the way! With just one unified log-in, our platform gives you access to multiple high-yield savings products, which you can track and manage with ease.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How CD laddering can help you diversify your savings",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "CD laddering may be another great way to maximize your interest-earning potential, especially when it comes to long-term savings. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "With a CD ladder, you spread your cash savings equally across multiple CD accounts, each with its own maturity date. For example, if you had $6,000 to save, you could put $2,000 in each of three CDs: a one-year, a two-year, and a three-year product.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "After year one, your one-year CD would mature. If you need the cash, you could utilize it at that time or reallocate it elsewhere. If you don’t need it right away, though, you can redeposit it into another three-year CD. A year later (at the end of year two), your two-year CD would mature, and you could choose to use those funds or deposit them into a three-year CD.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The goal is to eventually have all of your savings money spread across long-term CDs, which usually offer the highest returns. However, you’d also reach the point where each year, another one of your CDs matures, giving you consistent access to your cash savings if you need it. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "With a single account, SaveBetter customers can invest in FDIC-insured savings products — like CDs — from numerous partner banks with a wide range of maturities and returns. This makes it easy to shop around for the best new rates while also managing an array of deposit accounts in one place.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Be sure to diversify your cash savings",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "de3eb354-696e-48b7-b444-6b2a075e02a7",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Cash savings play an essential role in any successful financial plan, so be sure not to overlook them when planning for your retirement.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Whether you’re building out your first strategy or on the cusp of retirement, maintaining high-yield savings accounts and CDs is a safe and convenient way to balance out market volatility and still earn a healthy return on your deposits. Plus, you’ll also have cash on-hand if you need it, whether for upcoming spending or unexpected expenses.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "SaveBetter for your future",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "438d8302-cb50-4865-9aa9-7e7badbf1e56",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Select a CD That's Right for Your Needs",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/select-a-cd-for-your-needs",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Working Hard for Its Community: Inside Patriot Bank",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/about-patriot-bank",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The Top 5 Factors People Care Most About When Opening a Savings Account",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/top-5-factors-savings-account",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Open Joint Accounts with SaveBetter",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/how-to-open-joint-accounts",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Make a Money-Saving Resolution That Will Actually Stick",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/stick-to-your-money-saving-resolution",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        "title": "Related Articles",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": true,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      }
    ],
    "image": [
      {
        "alt": "Two beach chairs by the ocean",
        "img": "//a.storyblok.com/f/70134/1584x800/66405bca1a/retirement.jpg",
        "_uid": "d1d96d3a-4866-4885-ba0c-551c6b90539a",
        "component": "media"
      }
    ],
    "intro": "",
    "title": "Why Savings Accounts Should Be Part of Your Retirement Strategy ",
    "metadata": {
      "_uid": "ec810ca0-4250-4441-a610-1eb86a140eea",
      "title": "Why Savings Accounts Should Be Part of Your Retirement Strategy | SaveBetter",
      "plugin": "meta-fields",
      "description": "Consider cash as an integral part of your diversified retirement strategy, and here’s why."
    },
    "component": "blogPostPage",
    "categories": [
      "12716529-9eb5-408c-b8c8-8f5355832667",
      "6305ef4d-a589-447a-a4ca-5293dc446796"
    ],
    "disableIndexing": false,
    "blogPostEndingSection": [
      {
        "_uid": "c93e16f1-1e6f-49f8-b8f3-96a1a6a6d6a0",
        "links": ["Facebook", "LinkedIn", "Twitter2", "Email"],
        "footNote": "",
        "component": "blogPostEndingSection",
        "hasBorder": false,
        "sectionTitle": "Share this article",
        "hasBackground": false,
        "hasPaddingTop": true,
        "hasPaddingBottom": true
      }
    ]
  },
  "slug": "savings-accounts-for-retirement-strategy",
  "full_slug": "blogs/savings-accounts-for-retirement-strategy",
  "sort_by_date": null,
  "position": 20,
  "tag_list": [],
  "is_startpage": false,
  "parent_id": 290604867,
  "meta_data": null,
  "group_id": "6f7ce79d-5633-4873-931e-402e34a6f82d",
  "first_published_at": "2020-12-23T22:18:03.237Z",
  "release_id": null,
  "lang": "default",
  "path": null,
  "alternates": [],
  "default_full_slug": null,
  "translated_slugs": null
}

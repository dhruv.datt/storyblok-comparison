{
  "name": "How to Get Started With SaveBetter",
  "created_at": "2023-04-12T09:56:26.377Z",
  "published_at": "2023-02-14T16:07:44.949Z",
  "id": 290604972,
  "uuid": "a9752f1e-b83c-410d-8b0f-befd5b19ca7c",
  "content": {
    "_uid": "d9e1ba44-ca74-4adb-8a30-a33d658bcc9f",
    "body": [
      {
        "_uid": "c5aa956f-9f8c-4457-8814-c5c186076250",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "We’ve all heard the importance of diversification across our investment portfolios.   But when it comes to diversifying your day-to-day savings, there simply haven’t been many available solutions to help — until now.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "287377c1-1562-494d-a7c3-adb11f08ea99",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "SaveBetter.com",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com",
                        "uuid": null,
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " allows you to invest your savings across an exclusive network of FDIC-insured partner banks and NCUA-insured partner credit unions — all from one account. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You can get access to a variety of savings products, such as high-yield savings accounts, and traditional or no-penalty certificates of deposit (CDs).",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The SaveBetter platform gives you the following ",
                  "type": "text"
                },
                {
                  "text": "benefits and more",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/4-ways-savebetter-will-help-you-smash-your-savings-goals",
                        "uuid": "73eb2f1c-30b6-4735-bb7a-668a1505793e",
                        "anchor": null,
                        "custom": {},
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": ":", "type": "text" }
              ]
            },
            { "type": "paragraph" },
            {
              "type": "bullet_list",
              "content": [
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "SAFETY. You get high-yielding products held at federally insured financial institutions.",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "CHOICE. Access to quality products from a large network of banks sorted by yield, maturity date, and more to get the best product for you.",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "CONVENIENCE. You only need one account to access and manage your funds.",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Now that you’ve been introduced to SaveBetter, here’s how it works and how to get started diversifying your savings, all in a few steps:",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "3304e105-ccd7-4636-bca4-67fa793f9892",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "SaveBetter’s sign-up process is fast and simple, so you can efficiently move your cash into high-yield products at federally insured partner institutions.*",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Step 1: Check out available high-yield savings products",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "First, review the savings marketplace and choose a product for your initial investment. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You can see if there’s any minimum balance required to start, and how often interest is compounded and credited. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Products can be sorted by type (high-yield savings, fixed-term CDs, etc.):",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "How to get started with SaveBetter",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "63787625-c49d-4ff7-9b3d-0c639d599670",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Step 2: Sign up in minutes",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Proceed to the sign-up process. With SaveBetter, you can be up and running in minutes:",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "95b079a6-be4a-4eff-ba7d-5a6cbd08c166",
        "media": [
          {
            "alt": "SaveBetter registration page makes your sign-up process easy",
            "img": "//a.storyblok.com/f/180759/1200x1078/e0e263ffc0/registration-page-new.jpg",
            "_uid": "9a9cdf96-9639-479e-b5c1-0d5a2c8a4930",
            "component": "media"
          }
        ],
        "component": "blogPostImage",
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "4aa345e0-ea5f-4de2-9982-67021c649a39",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Step 3: Connect your bank or financial institution",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Next, link your financial institution to fund your SaveBetter account.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Your funds move from your financial institution to SaveBetter’s custodian bank, where it’s held safe and secure. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "From start to finish, your money will be with an federally insured financial institution, guaranteed for up to $250,000 per depositor per bank.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "f1b62644-3c26-4b62-b0fb-3f84b040d4b4",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "One-time registration",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You only need to register for a SaveBetter account the first time you move your money into a savings product. There’s no need to create additional accounts with SaveBetter in the future — even when you choose to save with products from different banks.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The sign-up process can be done online from the comfort and safety of your home, providing you with a smooth online banking experience.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "After you register for and fund your SaveBetter account, you can choose from several savings products offered by partner banks. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Track your accounts all from one place",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Even after you’ve diversified your savings within SaveBetter, rest assured, it’s easy to track and manage your savings from a single dashboard.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "It’s easy after you sign up",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "f4af58b5-7d82-457d-8e7c-089f8514697b",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Account management made simple",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Deposits and withdrawals can easily be made from your Account Overview page. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "And when one of your CDs reaches maturity, SaveBetter automatically sends the funds to your linked account.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "b44c26a9-a018-40ee-b325-f56b563441ed",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You don’t want to spend countless hours searching for the best savings products on the market. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "Time is money, after all.", "type": "text" }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "SaveBetter presents you with a variety of savings accounts and, coming soon, CDs so that you don’t have to waste time scouring the internet.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Save time searching for high-yield savings products",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "e9e3d148-bdc6-433f-a930-bde92645f1e2",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you diversify your savings through traditional means, opening up and maintaining a plethora of bank accounts, you would normally need to track down interest income statements (1099-INT) for each account come tax time.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                { "text": "Nobody wants that headache.", "type": "text" }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "With SaveBetter, you get all of your tax documents in one place, streamlining the process.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "A breeze come tax time",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "ac7efaae-4d92-446a-8c0a-fafd8f9f7744",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "SaveBetter also allows you to diversify your savings across high yield savings accounts, CDs (coming soon), and more.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "You can also easily implement a “laddering” savings strategy to decrease interest rate and reinvestment risk. Laddering involves investing in products with varying yields and maturities. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If yields rise, you won’t be locked into all of your savings for several years. But if yields fall, you won’t have to reinvest all your savings in lower-yielding products all at the same time.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Diversify your savings",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "306d2429-e53f-4fb3-8275-38577a0d3c6c",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                { "text": "To recap, the SaveBetter ", "type": "text" },
                {
                  "text": "savings marketplace",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                },
                {
                  "text": " decreases the time you spend diversifying your savings by:",
                  "type": "text"
                }
              ]
            },
            {
              "type": "bullet_list",
              "content": [
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        { "text": "Requiring only one account", "type": "text" }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Giving you all your savings product information on one dashboard",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Finding and presenting the best deals to you",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "list_item",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Providing you with one easy-to-read document at tax time",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "It does all of this without charging you fees.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "If you’re ready to revolutionize the way you save money, sign up for SaveBetter today.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "No hidden fees, period",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      }
    ],
    "image": [
      {
        "alt": "Couple taking photos of hot air balloons",
        "img": "//a.storyblok.com/f/70134/1584x800/af89ede024/blog-3-min.jpg",
        "_uid": "2f61c688-59a5-44bb-844f-36e76521e474",
        "component": "media"
      },
      {
        "alt": "",
        "img": "",
        "_uid": "4652f847-4e34-442c-85aa-8e7bc799ef0c",
        "component": "media"
      }
    ],
    "intro": "",
    "title": "How to get started with SaveBetter",
    "metadata": {
      "_uid": "0659d392-50ea-4424-855c-fef970d0b3d8",
      "title": "How to get started with SaveBetter",
      "plugin": "meta-fields",
      "description": "How to invest your savings across SaveBetter's exclusive network of FDIC-insured partner banks all from one account."
    },
    "component": "blogPostPage",
    "socialBox": [],
    "categories": [
      "12716529-9eb5-408c-b8c8-8f5355832667",
      "b2473d58-7208-4462-99cb-9dd7f654d999"
    ],
    "disableIndexing": false,
    "showBreadcrumbs": false,
    "blogPostEndingSection": [
      {
        "_uid": "d70cee46-d598-4c30-a0ca-2e2165e5df35",
        "links": ["Facebook", "LinkedIn", "Twitter2", "Email"],
        "footNote": "*Subject to successful identification verification.",
        "component": "blogPostEndingSection",
        "hasBorder": false,
        "sectionTitle": "Share this article",
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": true
      }
    ]
  },
  "slug": "how-to-get-started-with-savebetter",
  "full_slug": "blogs/how-to-get-started-with-savebetter",
  "sort_by_date": null,
  "position": 70,
  "tag_list": [],
  "is_startpage": false,
  "parent_id": 290604867,
  "meta_data": null,
  "group_id": "77e69a9e-3fb2-4855-a725-f8037c0fd198",
  "first_published_at": "2020-09-25T19:49:39.284Z",
  "release_id": null,
  "lang": "default",
  "path": null,
  "alternates": [],
  "default_full_slug": null,
  "translated_slugs": null
}

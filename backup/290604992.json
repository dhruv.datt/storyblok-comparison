{
  "name": "Working Hard for Its Community: Inside Patriot Bank",
  "created_at": "2023-04-12T09:56:26.377Z",
  "published_at": "2022-02-18T10:06:38.857Z",
  "id": 290604992,
  "uuid": "7dc69af5-c524-4bc8-878f-780cf939db0d",
  "content": {
    "_uid": "8a7065fb-85d1-4776-a960-b899c3661076",
    "body": [
      {
        "_uid": "5dcc7352-3473-4599-ae01-0097fc790e52",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Dozens of tight-knit enclaves scatter the corridor along Interstate 95 through the New England state of Connecticut. Towns like Greenwich, Norwalk and Hartford are hubs for growing families and small businesses. And a common fixture in many of these communities: A Patriot Bank branch. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "As a bank guided by its values and committed to standing up strong communities, Patriot Bank was a natural fit to join the network of partner banks offering their deposit products through SaveBetter. Here’s why savers nationwide should consider choosing to support Patriot Bank through SaveBetter.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "4f85ca27-b870-40d5-b155-5efe4fe3511f",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Headquartered in Stamford, Patriot Bank has been serving communities throughout Connecticut and in neighboring New York for over 25 years. Its mission since its founding in 1994 has been to help local families and businesses thrive. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "“Our clients are our neighbors, and we treat them that way,” says Robert G. Russell, Jr., President and Chief Executive Officer of Patriot Bank. “We really focus on customer service, and working for and within the community. Helping create a stronger, better-connected community has been the recipe for our success.” ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "That locally minded approach shows through in Patriot Bank’s lending practices. Loan officers are based locally, understand the economic landscape, and work with clients across the business spectrum ",
                  "type": "text"
                },
                {
                  "text": "—",
                  "type": "text",
                  "marks": [{ "type": "styled", "attrs": { "class": "" } }]
                },
                {
                  "text": " from for-profit to nonprofit institutions.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "A tradition of service ",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "e9eb8237-dfb6-41d3-8667-2dbb537daa7b",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Complementing the work it does to serve businesses, Patriot Bank and its staff also focus on directly supporting children’s programs that inspire kids to achieve their goals. The bank has given back through recent donations to institutions and causes that encourage students to stay in school and enable them to grow academically and professionally, including by connecting them with coaches and mentorship opportunities.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "The human side of community investment",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "25f8a29f-9090-4b20-b6c8-9e90c587e77b",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "What is Patriot Bank offering through the SaveBetter platform? One of the most competitive deposit products available: a ",
                  "type": "text"
                },
                {
                  "text": "money market deposit account",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/bank/patriot-bank/patriot-bank-high-yield-money-market-deposit-account",
                        "uuid": "f8afde0a-65ca-40c9-8f83-3d14bc9fc6ca",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " (MMDA)with an APY of 0.45% — a rate that’s nearly six times higher than the national average for a traditional savings account.* (Not familiar with MMDAs and their unique benefits? Learn more ",
                  "type": "text"
                },
                {
                  "text": "here",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/blogs/what-is-a-money-market-account",
                        "uuid": "ff34648a-3fcb-44f3-b8a3-5bad5a6daa00",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                { "text": ".)", "type": "text" }
              ]
            }
          ]
        },
        "title": "Patriot Bank’s high-earning money market deposit account ",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "ba0043c4-9051-4250-947c-0e8fdcf42276",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "The Federal Deposit Insurance Corporation (FDIC) protects bank customers by keeping their money protected against losses related to a bank failure. Because Patriot Bank is an FDIC member bank, money you hold through your SaveBetter account in a Patriot Bank deposit product (like the previously mentioned MMDA) is covered by deposit insurance through the FDIC, up to applicable limits. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Ready to save with Patriot Bank? Here’s your next move: Check out the bank’s ",
                  "type": "text"
                },
                {
                  "text": "savings solutions",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/bank/patriot-bank/patriot-bank-high-yield-money-market-deposit-account",
                        "uuid": "f8afde0a-65ca-40c9-8f83-3d14bc9fc6ca",
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "story"
                      }
                    }
                  ]
                },
                {
                  "text": " and select a product that’s right for you.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Patriot Bank is FDIC insured",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "afef399a-8dce-450a-8c41-f6d739f54614",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Build a Nest Egg, in a Nutshell",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/build-nest-egg-in-nutshell",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to maintain your post-pandemic saving habits",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/maintain-post-pandemic-savings-habits",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Diversify Your Cash Savings",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/diversify-your-cash-savings",
                        "uuid": null,
                        "anchor": null,
                        "target": "_self",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How to Select a CD That's Right for Your Needs",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/select-a-cd-for-your-needs",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "How APY Helps Your Savings Grow",
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "https://www.savebetter.com/blogs/how-apy-helps-your-savings-grow",
                        "uuid": null,
                        "anchor": null,
                        "target": "_blank",
                        "linktype": "url"
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        "title": "Related Articles",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": true,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      }
    ],
    "image": [
      {
        "alt": "A women and a man looking at a laptop together",
        "img": "//a.storyblok.com/f/70134/1584x800/1f641b4d46/patriot-bank-blog.jpg",
        "_uid": "d1d96d3a-4866-4885-ba0c-551c6b90539a",
        "mobile": "",
        "tablet": "",
        "desktop": "",
        "component": "media"
      }
    ],
    "intro": "Connecticut-based Patriot Bank has 25-plus years of experience serving its community. And its commitment extends to philanthropy, focused on helping kids excel. ",
    "title": "Working Hard for Its Community: Inside Patriot Bank",
    "metadata": {
      "_uid": "ec810ca0-4250-4441-a610-1eb86a140eea",
      "title": "About Patriot Bank | SaveBetter",
      "plugin": "meta-fields",
      "description": "Patriot Bank offers competitive high-yield deposit products through SaveBetter. Learn more about this Connecticut-based bank’s commitment to community. "
    },
    "component": "blogPostPage",
    "categories": [
      "12716529-9eb5-408c-b8c8-8f5355832667",
      "e87b7c70-987d-40ec-af32-1ac44c4b1fd6"
    ],
    "disableIndexing": false,
    "blogPostEndingSection": [
      {
        "_uid": "c93e16f1-1e6f-49f8-b8f3-96a1a6a6d6a0",
        "links": ["Facebook", "LinkedIn", "Twitter2", "Email"],
        "footNote": "* Based on FDIC national rate for non-jumbo deposits under $100,000, as of January 18, 2022. ",
        "component": "blogPostEndingSection",
        "hasBorder": false,
        "sectionTitle": "Share this article",
        "hasBackground": false,
        "hasPaddingTop": true,
        "hasPaddingBottom": true
      }
    ]
  },
  "slug": "about-patriot-bank",
  "full_slug": "blogs/about-patriot-bank",
  "sort_by_date": null,
  "position": 20,
  "tag_list": [],
  "is_startpage": false,
  "parent_id": 290604867,
  "meta_data": null,
  "group_id": "4f15190b-4a8b-4ac7-9fca-009341a464cb",
  "first_published_at": "2020-12-23T22:18:03.000Z",
  "release_id": null,
  "lang": "default",
  "path": null,
  "alternates": [],
  "default_full_slug": null,
  "translated_slugs": null
}

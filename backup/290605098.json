{
  "name": "Banks and Credit Unions: What’s the Difference?",
  "created_at": "2023-04-12T09:56:30.576Z",
  "published_at": "2023-02-28T20:35:33.060Z",
  "id": 290605098,
  "uuid": "532268b2-6bef-44f7-be2d-ff270bb45c6c",
  "content": {
    "_uid": "de73d039-9721-410d-b02d-81bbc768e770",
    "body": [
      {
        "_uid": "748468e2-d224-41c2-82e8-2dddecbb9a69",
        "kind": "1",
        "text": "Banks vs. Credit Unions: What’s the Difference?",
        "block": "",
        "color": "text-black",
        "level": "1",
        "component": "heading",
        "hasBackground": false,
        "hasPaddingTop": true,
        "hasPaddingBottom": false
      },
      {
        "_uid": "b99de019-d6ab-4fd3-8bfd-d31cbb64596e",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "heading",
              "attrs": { "level": 1 },
              "content": [
                {
                  "text": "Think banks and credit unions are the same thing? Think again. Knowing the differences can help you decide where to put your money.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "7338673a-7807-4bad-b79c-d72f064ac329",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Savers looking to park their cash somewhere safe have a wealth of options. Two popular choices are banks and credit unions. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Although these financial institutions share a lot of common ground — both offer a range of financial services such as checking and savings accounts, loans and business accounts, and both typically are federally insured — they’re not identical in how they operate and what they provide. In fact, understanding how the two compare can help you make the right choice for your financial needs. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Let’s take a closer look at banks and credit unions. And further below, you’ll find information about how SaveBetter makes it easy to add both bank and credit union savings products to your portfolio.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "a803ed99-c737-476c-910f-7214b6d19e20",
        "cards": [
          {
            "_uid": "3cf3ac1f-732c-45d5-b456-5144c0d9dcf0",
            "left": "Banks are typically for-profit financial insitituions",
            "right": "Credit unions are typically not-for profit.",
            "component": "whySaveBetterCard"
          },
          {
            "_uid": "043e335c-51ce-47eb-80ce-dc06d4d79a82",
            "left": "Banks are typically shareholder-owned.",
            "right": "Credit unions are typically member-owned.",
            "component": "whySaveBetterCard"
          },
          {
            "_uid": "fcd60e05-0c01-4a36-9d4f-67e469f1b513",
            "left": "Banks usually have few, if any, restrictions on who can become a depositor.",
            "right": "Credit unions generally require depositors become members.",
            "component": "whySaveBetterCard"
          },
          {
            "_uid": "c786aa75-71d9-4e93-b2ea-44d6db5e551e",
            "left": "Banks can be covered by FDIC deposit insurance.",
            "right": "Credit unions can be covered by NCUA deposit insurance.",
            "component": "whySaveBetterCard"
          }
        ],
        "title": "Key Differences Between Banks and Credit Unions",
        "component": "whySaveBetter",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "leftColumnTitle": "Banks",
        "hasPaddingBottom": false,
        "rightColumnTitle": "Credit Unions"
      },
      {
        "_uid": "76a37620-1f5e-4100-b471-c146c050e79b",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Business model: ",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": "Unlike credit unions, banks are for-profit institutions. Their primary focus is on delivering value for their shareholders or for private owners.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Ownership:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " Banks for-profit businesses, and are either privately owned or publicly traded. When a bank is publicly traded, such as on the stock market, each shareholder is a part owner. Shareholders take part in the bank’s success in the form of dividends and stock appreciation, and banks are run to maximize the value of the business for those shareholders. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Clientele:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " The customer base of a bank may depend on its size and physical location. Some banks are national in reach, while others aim to service a local community. Some bigger banks may offer certain products and services only in specific geographic areas, such as only within or only outside the areas surrounding their physical branches. The accessibility of bank products may be based on a customer’s eligibility criteria; for loans or credit cards, for instance, a customer’s credit history may be considered. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Offering:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " For individual consumers, banks offer deposit products like checking, savings and retirement accounts, and many sell credit cards and loans, including mortgages. Some banks also have divisions that provide investment advisory services. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Insurance:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " Banks are a safe place to keep your money, as deposits are insured against loss by the Federal Deposit Insurance Corp. (FDIC). Let’s say you have an individual savings account with an FDIC member bank; up to $250,000 held in that account would automatically have FDIC coverage.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "What to Know About Banks",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": true,
        "hasPaddingBottom": false
      },
      {
        "_uid": "42dfab0e-d0ef-4d0a-b237-c785eadc77f7",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Business model:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " Credit unions are not-for-profit operations that are owned and operated by the people who belong to the organizations and use their services. For that reason, “member” is the accepted term for a person who banks at a credit union. While banks are in the business of making a profit for their shareholders, credit unions are in the service of the people who use them, also known as members.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Thanks to their nonprofit status, credit unions are tax-exempt. Credit Unions don’t pay the taxes that banks are subject to. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Ownership: ",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": "Credit unions are member-owned. When you become a member of a credit union, you effectively become a part owner. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Clientele:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " Membership is required to use a credit union. Much like banks, credit unions come in a variety of sizes, and their addressable market could be national, regional, or geared toward just one state or community. Moreover, some credit unions serve specific groups or industries. Some may limit access to membership to those who work in a given field, such as education, and to members of those workers’ families. Enrolling members may have to pay a credit union a one-time fee (or donation) in order to join. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Offering:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " Credit unions usually offer many comparable products and services to those offered by banks. You may encounter different names for very similar products depending on whether you bank with a credit union or bank. Learn more about banks’ and credit unions’ differing terminology below.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Insurance:",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": " Your money is protected in a credit union. Deposits up to $250,000 are automatically insured against loss by the National Credit Union Administration (NCUA), a federal institution. That’s the same level of protection you’d get at a bank.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "What to Know About Credit Unions",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "3bd15075-b05e-49e0-be24-f9a3807f68b9",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Depositor vs. member. ",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": "A client of a credit union who holds their savings with the institution is commonly called a “member,” reflecting the fact that each client is also a member (and part owner) of the not-for-profit organization. Bank customers who deposit their cash with the institution are known as — no surprise here — depositors. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Savings account vs. share account. ",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": "Many people will already be familiar with the banking term “savings account,” which refers to the type of interest-bearing deposit account banks offer that pay interest on the funds they hold. In credit union terminology, this type of account is often called a regular share or a share account. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Certificate of deposit vs. share certificate. ",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": "A bank may offer a type of term deposit product called a certificate of deposit, or CD for short. CDs offer a set interest rate for a set period of time, during which the funds in the CD account are not accessible. At the end of that time period, the CD reaches maturity and the CD account holder can access the principal amount plus the accrued interest. Some credit unions offer a similar type of product under the names share certificate and certificate account certificate. In the case of share certificates and the like, the earnings the account holder accrues are known as dividends.",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Interest vs. dividend. ",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                },
                {
                  "text": "You typically won’t see a credit union advertising the interest rate its share account pays. That’s because in the parlance of credit unions, a share account pays dividends, not interest. In this way, the terminology mirrors the world of stocks: a public company’s stockholders are paid dividends out of the company’s profits. Members of a credit union are effectively shareholders in the institution, and they earn dividends on the funds they hold there. That said, APY, or annual percentage yield, is a common term across both banks and credit unions stating the expected yearly earnings of an interest- or dividend-bearing account, expressed as a rate.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "Bank vs. Credit Union Terminology ",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": false
      },
      {
        "_uid": "c563e123-2d31-43d6-a126-d040edf151c4",
        "text": {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Choosing the best place to stash your money is an important decision. And arriving at the answer isn’t always so simple. That’s why SaveBetter offers you access to a diverse, exclusive selection of FDIC-insured banks and NCUA-insured credit unions. You may find that both banks and credit unions appeal to the saver in you. Here are a few reasons why: ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "A Single Login for Bank and Credit Union Products",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "With one unified login, you can safely explore your options from our always-growing inventory and confidently choose the ones that are right for you. Within your SaveBetter dashboard, you’ll be able to see products you’ve acquired from the banks and credit unions we partner with, all in one view. ",
                  "type": "text"
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Seamless, No-cost Credit Union Membership",
                  "type": "text",
                  "marks": [{ "type": "bold" }]
                }
              ]
            },
            {
              "type": "paragraph",
              "content": [{ "text": " ", "type": "text" }]
            },
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Obtaining a savings product offered by a credit union through SaveBetter is a streamlined process where becoming a credit union member is seamlessly integrated. SaveBetter customers don’t pay any membership fees or donations when they deposit money into a credit union savings product available through the platform. You save money, and it’s convenient. That’s what SaveBetter is all about.",
                  "type": "text"
                }
              ]
            }
          ]
        },
        "title": "SaveBetter Can Help You Find the Banks or Credit Unions That Are Right for You",
        "component": "blogPostSection",
        "hasBorder": false,
        "hasBackground": false,
        "hasPaddingTop": false,
        "hasPaddingBottom": true
      }
    ],
    "layout": {
      "id": "d64b2f44-1440-4125-882c-1ac2928c5067",
      "url": "",
      "linktype": "story",
      "fieldtype": "multilink",
      "cached_url": "layouts/explore-product-layout"
    },
    "metadata": {
      "_uid": "e4280f7e-579f-4cc5-b597-06dfaee9b18b",
      "title": "Banks vs. Credit Unions: What's the Difference? | SaveBetter",
      "plugin": "meta-fields",
      "description": "Interest rates, fees, and insurance: We've compiled the pros and cons of banks and credit unions. Discover the major similarities and differences to help you decide."
    },
    "component": "newBlogHomePage",
    "Categories": [],
    "showBreadcrumbs": false,
    "blocksearchengines": false
  },
  "slug": "credit-unions-vs-banks",
  "full_slug": "credit-unions/credit-unions-vs-banks",
  "sort_by_date": null,
  "position": 10,
  "tag_list": [],
  "is_startpage": false,
  "parent_id": 290604892,
  "meta_data": null,
  "group_id": "5d688a2d-0fe4-456f-b64f-25fbefaf1c92",
  "first_published_at": "2022-06-09T19:08:39.147Z",
  "release_id": null,
  "lang": "default",
  "path": null,
  "alternates": [],
  "default_full_slug": null,
  "translated_slugs": null
}
